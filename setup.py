from setuptools import setup, find_packages

setup(
	name='app_template',
	version='0.4.0',
	packages=find_packages(),
	url='https://gitlab.com/ptl-public/app-template-python',
	author='Max von Tettenborn',
	author_email='max.von.tettenborn@getportal.org',
	description='A fully functional and minimal Portal app that can be used to quickly bootstrap a new app using Python',
	install_requires=[
		'gconf',
		'fastapi',
		'uvicorn',
		'httpx',
		'tinydb',
	],
	extras_require={
		'dev': [
			'pytest',
			'requests',
		]
	},
)
