# Build
FROM python:3.8 as build

RUN python3 -m venv /venv
COPY . /project
RUN /venv/bin/pip install /project

# Production
FROM python:3.8 as production

COPY --from=build /venv /venv
COPY config.yml .
COPY portal_meta/ portal_meta/
RUN mkdir /data

HEALTHCHECK --start-period=5s CMD curl -f localhost/public/status || exit 1
EXPOSE 80
CMD [ "venv/bin/uvicorn", "app_template:create_app", "--factory", "--host", "0.0.0.0", "--port", "80" ]