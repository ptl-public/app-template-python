def test_get_status(api_client):
	first_reply = api_client.get('public/status').json()
	assert first_reply['status'] == 'OK'

	second_reply = api_client.get('public/status').json()
	assert second_reply['nr_of_calls'] == first_reply['nr_of_calls'] + 1
