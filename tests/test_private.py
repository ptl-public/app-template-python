def test_get_status(api_client):
	first_reply = api_client.get('/', headers={'X-Ptl-Foo': 'foo'}).json()
	assert 'message' in first_reply
	assert first_reply["x_ptl_headers"]['x-ptl-foo'] == 'foo'

	second_reply = api_client.get('/').json()
	assert second_reply['nr_of_calls'] == first_reply['nr_of_calls'] + 1
