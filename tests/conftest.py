import logging

import gconf
import pytest
from fastapi.testclient import TestClient

import app_template


@pytest.fixture
def api_client(tempfile_db_config) -> TestClient:
	yield TestClient(app_template.create_app())


@pytest.fixture
def tempfile_db_config(tmp_path):
	db_file = tmp_path / 'db'
	print(f'Using database: {db_file}')
	with gconf.override_conf({'database': {'filename': db_file}}):
		yield
