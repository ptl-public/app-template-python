import logging
import sys
from importlib.metadata import metadata

import gconf
from fastapi import FastAPI

from . import database
from .web import public, private, peer

log = logging.getLogger(__name__)


def create_app():
	loaded_config = gconf.load('config.yml')
	_configure_logging()
	log.info(f'loaded config {loaded_config}')

	database.init_database()
	log.info('Initialized DB')

	app_meta = metadata('app_template')
	app = FastAPI(
		title='App Template',
		description=app_meta['summary'],
		version=app_meta['version'],
		redoc_url='/openapi',
	)
	app.include_router(public.router, prefix='/public')
	app.include_router(private.router)
	app.include_router(peer.router, prefix='/peer')

	return app


def _configure_logging():
	logging.basicConfig(
		format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
		handlers=[logging.StreamHandler(sys.stdout)])
	for module, level in gconf.get('log', default=dict()).items():  # type: str, str
		logger = logging.getLogger() if module == 'root' else logging.getLogger(module)
		logger.setLevel(getattr(logging, level.upper()))
