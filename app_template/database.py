from contextlib import contextmanager
from pathlib import Path
from tinydb import Query
import gconf
from tinydb import TinyDB


def init_database():
	file = Path(gconf.get('database.filename'))
	if file.is_dir():
		raise Exception(f'{file} is a directory, should be a file or not existing')
	file.parent.mkdir(exist_ok=True, parents=True)
	file.touch(exist_ok=True)

	with get_db() as db:
		# We want to count how often each endpoint is called
		counter = Query()
		for path in ['/', '/public/status']:
			if not db.search(counter.path == path):
				db.insert({'path': path, 'count': 0})


@contextmanager
def get_db() -> TinyDB:
	with TinyDB(gconf.get('database.filename')) as db_:
		yield db_
