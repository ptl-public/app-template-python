import logging

from fastapi import APIRouter, status
from tinydb import Query
from tinydb.operations import increment
from app_template.database import get_db

log = logging.getLogger(__name__)

router = APIRouter()


@router.get('/status', status_code=status.HTTP_200_OK)
def get_status():
	counter = Query()
	with get_db() as db:
		db.update(increment('count'), counter.path == '/public/status')
		current_count = db.search(counter.path == '/public/status')[0]['count']
	return {
		'status': 'OK',
		'nr_of_calls': current_count,
	}
