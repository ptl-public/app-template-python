import logging

from fastapi import APIRouter, Depends
from starlette import status

from app_template.web.dependencies import AuthValues

log = logging.getLogger(__name__)

router = APIRouter()


@router.get('/status', status_code=status.HTTP_200_OK)
def get_status(auth_values: AuthValues = Depends(AuthValues)):
	log.debug(f'request from {auth_values.id}')
	return {
		'message': f'This endpoint was called from {auth_values.name} with id {auth_values.id}.',
		'x_ptl_headers': auth_values.x_ptl_headers,
	}
