from enum import Enum, unique

from fastapi import Header
from starlette.requests import Request


class AuthValues:
	def __init__(
			self,
			request: Request,
			x_ptl_client_type: str = Header(None),
			x_ptl_client_id: str = Header(None),
			x_ptl_client_name: str = Header(None),
	):
		self.type = self.ClientType(x_ptl_client_type) if x_ptl_client_type else self.ClientType.ANONYMOUS
		self.id = x_ptl_client_id
		self.name = x_ptl_client_name
		self.x_ptl_headers = {k: v for k, v in request.headers.items() if k.lower().startswith('x-ptl-')}

	@unique
	class ClientType(Enum):
		TERMINAL = 'terminal'
		PEER = 'peer'
		ANONYMOUS = 'anonymous'
