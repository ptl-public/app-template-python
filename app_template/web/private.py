import logging

import httpx
from fastapi import APIRouter, status, Depends
from httpx import HTTPStatusError
from tinydb import Query
from tinydb.operations import increment
from app_template.database import get_db
from app_template.web.dependencies import AuthValues

log = logging.getLogger(__name__)

router = APIRouter()


@router.get('/', status_code=status.HTTP_200_OK)
def get_home(auth_values: AuthValues = Depends(AuthValues)):
	counter = Query()
	with get_db() as db:
		db.update(increment('count'), counter.path == '/')
		current_count = db.search(counter.path == '/')[0]['count']
	return {
		'message': f'This endpoint was called from {auth_values.name} with id {auth_values.id}.',
		'nr_of_calls': current_count,
		'x_ptl_headers': auth_values.x_ptl_headers,
	}


@router.get('/peers', status_code=status.HTTP_200_OK)
async def get_peers():
	async with httpx.AsyncClient() as client:
		response = await client.get('http://portal_core/protected/peers')
	return response.json()


@router.get('/peer_status/{p}', status_code=status.HTTP_200_OK)
async def get_peer_status(p: str):
	url = f'http://portal_core/internal/call_peer/{p}/peer/status'
	log.debug(f'calling {url}')
	async with httpx.AsyncClient() as client:
		response = await client.get(url)
		log.debug(f'response: {response.status_code}')
	try:
		response.raise_for_status()
	except HTTPStatusError:
		return {'response code': response.status_code}
	else:
		return response.json()
